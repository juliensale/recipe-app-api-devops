resource "aws_s3_bucket" "app_public_files" {
  bucket        = "${local.prefix}-files-juliensale"
  acl           = "public-read"
  force_destroy = true # Warning: if deleted, all the files inside the s3 bucket will also be deleted
}
